const dbHandler = require('./db_handler')
const User = require('../models/UserModel')

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
    await dbHandler.connect()
})

/**
 * Clear all test data after every test.
 */
afterEach(async () => {
    await dbHandler.clearDatabase()
})

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
    await dbHandler.closeDatabase()
})

const userComplete = {
    name: 'Lisaaa',
    gender: 'M'
}

describe('user ', () => {
    it('สามารถเพิ่ม user ได้', async () => {
        let error = null
        try {
            const user = new User(userComplete)
            await user.save()
        } catch (e) {
            error = e
        }
        expect(error).toBeNull()
    })
})

const userErrorNameEmpty = {
    name: '',
    gender: 'M'
}

describe('user ', () => {
    it('ไม่สามารถเพิ่ม user ได้ เพราะ name เป็นช่องว่าง', async () => {
        let error = null
        try {
            const user = new User(userErrorNameEmpty)
            await user.save()
        } catch (e) {
            error = e
        }
        expect(error).not.toBeNull()
    })
})

const userErrorNameLength = {
    name: 'Li',
    gender: 'F'
}

describe('user ', () => {
    it('ไม่สามารถเพิ่ม user ได้ เพราะ name ต้องมากกว่า 2', async () => {
        let error = null
        try {
            const user = new User(userErrorNameLength)
            await user.save()
        } catch (e) {
            error = e
        }
        expect(error).not.toBeNull()
    })
})
const userErrorGenderValid = {
    name: 'Lisayaa',
    gender: 'A'
}

describe('user ', () => {
    it('ไม่สามารถเพิ่ม user ได้ เพราะ เพศ ต้องเป็น M หรือ F', async () => {
        let error = null
        try {
            const user = new User(userErrorGenderValid)
            await user.save()
        } catch (e) {
            error = e
        }
        expect(error).not.toBeNull()
    })
})
const userErroDupicate = {
    name: 'HBDLizaa',
    gender: 'M'
}

describe('user ', () => {
    it('ไม่สามารถเพิ่ม user เพราะ name ซ้ำกัน ', async () => {
        let error = null
        try {
            const user = new User(userErroDupicate)
            await user.save()
            const user2 = new User(userErroDupicate)
            await user2.save()
        } catch (e) {
            error = e
        }
        expect(error).not.toBeNull()
    })
})
