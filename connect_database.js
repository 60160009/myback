const mongoose = require('mongoose')

mongoose.connect('mongodb://root:password@localhost/mydb', {
  useNewUrlParser: true,
  seUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function () {
  console.log('connect')
})
const kittySchema = new mongoose.Schema({
  name: String
})
kittySchema.methods.speak = function () {
  const greeting = this.name
    ? 'Meaw name is ' + this.name
    : "I don't have a name"
  console.log(greeting)
}

const Kitten = mongoose.model('kitten', kittySchema)
// const silence = new Kitten({ name: 'Silence' })
const fluffy = new Kitten({ name: 'fluffy' })
fluffy.speak()
fluffy.save(function (err, cat) {
  if (err) return console.error(err)
  cat.speak()
})
