const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  name: String,
  name: {
    type: String,
    required: true,      
    unique: true,
    minlength: 3

  },
  gender: {
    type: String,
    enum: ['M', 'F']
  }
})

module.exports = mongoose.model('users', userSchema)
