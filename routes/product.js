var express = require('express');
var router = express.Router();

// mock data
const products = [{
    id: '1001',
    name: 'Node.js for Beginners',
    category: 'Node',
    price: 990
}, {
    id: '1002',
    name: 'React 101',
    category: 'React',
    price: 3990
}, {
    id: '1003',
    name: 'Getting started with MongoDB',
    category: 'MongoDB',
    price: 1990
}]

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.json(products)
});

router.get('/:id', function (req, res, next) {
    let product = products.find(data => data.id == req.params.id)
    res.json(product)
});

router.post('/', function (req, res, next) {
    products.push({ ...req.body })
    res.json(products)
});

router.put('/', function (req, res, next) {
    const payload = req.body
    let productIndex = products.findIndex(data => data.id == req.params.id)
    products.splice(productIndex, 1, payload)
    res.json(products)
});

router.delete('/:id', function (req, res, next) {
    let productIndex = products.findIndex(data => data.id == req.params.id)
    products.splice(productIndex, 1)
    res.json(products)
});

module.exports = router;
